-- Run: hive -f development.sql

-- Not all tables needed for development are created here. Some of the
-- tables are created using this script
-- https://gitlab.wikimedia.org/repos/research/knowledge-gaps/-/blob/main/interactive/hive/development.sql.
-- Since knowledge-gaps depends on article-quality, there needs to be
-- some coupling between these two repositories and their data. That's
-- why you need to run the above script first, and then run this one.


SET hive.mapred.mode = nonstrict;
SET hive.exec.dynamic.partition.mode = nonstrict;
SET hive.strict.checks.large.query = false;
SET mapreduce.map.memory.mb = 8192;
SET mapreduce.map.java.opts=-Xmx7372m;
SET mapreduce.reduce.memory.mb = 8192;
SET mapreduce.reduce.java.opts=-Xmx7372m;
SET mapreduce.job.reduces=64;


DROP TABLE IF EXISTS bmansurov.mediawiki_project_namespace_map;
CREATE TABLE bmansurov.mediawiki_project_namespace_map AS
SELECT *
FROM wmf_raw.mediawiki_project_namespace_map
WHERE snapshot="2022-04";
SELECT COUNT(1) FROM bmansurov.mediawiki_project_namespace_map;


DROP TABLE IF EXISTS bmansurov.mediawiki_wikitext_current;
CREATE TABLE bmansurov.mediawiki_wikitext_current AS
SELECT mwc.*
FROM bmansurov.mediawiki_page_history mph
INNER JOIN wmf.mediawiki_wikitext_current mwc
ON mwc.page_id = mph.page_id
    AND mwc.wiki_db = mph.wiki_db
WHERE mwc.snapshot="2022-04"
    AND mwc.page_namespace=0
LIMIT 50000;
SELECT COUNT(1) FROM bmansurov.mediawiki_wikitext_current;


DROP TABLE IF EXISTS bmansurov.wikidata_item_page_link;
CREATE TABLE bmansurov.wikidata_item_page_link AS
SELECT wipl.*
FROM bmansurov.mediawiki_page_history mph
INNER JOIN wmf.wikidata_item_page_link wipl
ON wipl.page_id = mph.page_id
    AND wipl.wiki_db = mph.wiki_db
WHERE wipl.snapshot="2022-03-28"
    AND wipl.page_namespace=0
LIMIT 50000;
SELECT COUNT(1) FROM bmansurov.wikidata_item_page_link;
