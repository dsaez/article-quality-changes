FEATURES_TABLE = "bmansurov.article_quality_features_20220323"
SCORES_TABLE = "bmansurov.article_quality_scores_20220323"
MEDIAWIKI_SNAPSHOT = "2022-04"
WIKIDATA_SNAPSHOT = "2022-03-28"
MODE = "production"

egg:
	python setup.py bdist_egg

submit: egg
	spark2-submit \
	--master yarn \
	--deploy-mode cluster \
	--driver-memory 4G \
	--executor-memory 16G \
	--executor-cores 4 \
	--py-files dist/article_quality-*.egg \
	article_quality/app.py \
	--features_table $(FEATURES_TABLE) \
	--scores_table $(SCORES_TABLE) \
	--mediawiki_snapshot $(MEDIAWIKI_SNAPSHOT) \
	--wikidata_snapshot $(WIKIDATA_SNAPSHOT) \
	--mode $(MODE)
