import argparse
import sys

from pyspark.sql import SparkSession
from article_quality import quality_model


def _generate_data(spark, mediawiki_snapshot, wikidata_snapshot,
                   features_table, scores_table, persist=True,
                   mode='production'):
    """Generate quality scores and return them as a DF. If persist is
    true save intermediate and final results in tables.

    Parameters
    ----------
    spark : SparkSession

    mediawiki_snapshot: str
        MediaWiki snapshot in the format '2022-01'.

    wikidata_snapshot: str
        Wikidata snapshot in the format '2022-01-24'.

    features_table : str
        Table (namespaced with the database name) for saving features.
        Will be used as a temporary view name if `persist` is False.

    scores_table : str
        Table (namespaced with the database name) for saving quality
        scores. Will be used as a temporary view name if `persist` is
        False.

    persist: bool
        Whether to save the results in `features_table` and
        `scores_table`. If false these tables will be temporary views.

    mode: str
        Either "production" or "development". Used to decide on the
        kinds of tables to retrieve data from. In "development" mode
        smaller tables are used for fast compute.

    Returns
    -------
        root
         |-- page_id: long (nullable = true)
         |-- item_id: string (nullable = true)
         |-- pred_qual: float (nullable = true)
         |-- wiki_db: string (nullable = true)

    """
    if mode == 'development':
        project_namespace_map_table = 'bmansurov.mediawiki_project_namespace_map'
        page_table = 'bmansurov.mediawiki_page'
        wikitext_current_table = 'bmansurov.mediawiki_wikitext_current'
        item_page_link_table = 'bmansurov.wikidata_item_page_link'
    else:
        project_namespace_map_table = 'wmf_raw.mediawiki_project_namespace_map'
        page_table = 'wmf_raw.mediawiki_page'
        wikitext_current_table = 'wmf.mediawiki_wikitext_current'
        item_page_link_table = 'wmf.wikidata_item_page_link'

    if persist:
        quality_model.create_features_table(spark, features_table)
        quality_model.create_scores_table(spark, scores_table)

    df = quality_model.generate_features(
        spark, mediawiki_snapshot, project_namespace_map_table,
        page_table, wikitext_current_table
    )
    quality_model.save_df(df, features_table, persist)

    df = quality_model.generate_scores(
        spark, mediawiki_snapshot, wikidata_snapshot, features_table,
        project_namespace_map_table, page_table, item_page_link_table
    )
    if persist:
        quality_model.save_df(df, scores_table)
    return df


def get_quality_scores(spark, mediawiki_snapshot, wikidata_snapshot,
                       mode='production'):
    """Generate and return article quality scores for immediate use
    (i.e. results won't be saved persistently). Wrapper around
    `_generate_data()` for easy use.

    Parameters
    ----------
    spark : SparkSession

    mediawiki_snapshot: str
        MediaWiki snapshot in the format '2022-01'.

    wikidata_snapshot: str
        Wikidata snapshot in the format '2022-01-24'.

    mode: str
        Either "production" or "development". Used to decide on the
        kinds of tables to retrieve data from. In "development" mode
        smaller tables are used for fast compute.

    Returns
    -------
        root
         |-- page_id: long (nullable = true)
         |-- item_id: string (nullable = true)
         |-- pred_qual: float (nullable = true)
         |-- wiki_db: string (nullable = true)
    """
    return _generate_data(spark, mediawiki_snapshot, wikidata_snapshot,
                          'features', 'scores', False, mode)


def main(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('--features_table',
                        default='bmansurov.article_quality_features_20220323',
                        help='Table to use for storing article features. '
                        'Must be namespaced with the database name. '
                        'Defaults to '
                        '"bmansurov.article_quality_features".')
    parser.add_argument('--scores_table',
                        default='bmansurov.article_quality_scores_20220323',
                        help='Table to use for storing article quality '
                        'scores. Must be namespaced with the database '
                        'name. Defaults to '
                        '"bmansurov.article_quality_scores".')
    parser.add_argument('--mediawiki_snapshot',
                        default='2021-11',
                        help='Mediawiki snapshot. Defaults to '
                        '"2021-11".')
    parser.add_argument('--wikidata_snapshot',
                        default='2021-12-13',
                        help='Wikidata snapshot. Defaults to '
                        '"2021-12-13".')
    parser.add_argument('--mode',
                        choices=['production', 'development'],
                        default='production',
                        help='Mode to run the script in. In production '
                        'mode data will be generated using production '
                        'tables, and in development mode smaller tables '
                        'will be used to generate data.')
    args = parser.parse_args()
    spark = SparkSession.builder.getOrCreate()
    _generate_data(spark, args.mediawiki_snapshot,
                   args.wikidata_snapshot, args.features_table,
                   args.scores_table, args.mode)


if __name__ == '__main__':
    sys.exit(main())

