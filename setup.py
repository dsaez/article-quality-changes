from setuptools import (
    find_packages,
    setup
)

setup(
    name="article_quality",
    version="0.0.1",
        install_requires=[
            "pyspark[sql]==2.4.4"
            # "wmfdata @ git+https://github.com/wikimedia/wmfdata-python.git@release",
    ],
    packages=find_packages(),
    python_requires=">=3.7",
    author="Wikimedia Foundation Research team",
    license="BSD 3-Clause"
)
